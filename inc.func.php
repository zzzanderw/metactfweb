<?php
//define password gen function
function password_gen($length, $uname, $challenge) {
	$salt = date('y');
	$shell = shell_exec("echo $uname $challenge $salt | sha512sum | base64 | head -1 | cut -c 1-$length");
	$shelltrim = trim($shell);
	return $shelltrim;
}
?>

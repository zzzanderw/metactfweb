<?php
require_once('../class.sqlite.php');
require_once('../inc.func.php');

//get dbname, username and password from post request
$username = $_POST['username'];
$password = $_POST['password'];
$dbuser = base64_decode($_COOKIE['loggedin']);

//check to make sure they aren't trying to do any SQL funny business
$sqlcheckusername = stristr($username, ';');
$sqlcheckpassword = stristr($password, ';');
if ($sqlcheckusername !== FALSE || $sqlcheckpassword !== FALSE) {
	exit("You can't use SQL on this challenge. Hint: strcmp behaves, um, interestingly, when given null parameters. <a href='index.html'>Try again</a>");
}

//define sqlite object
$sqlite = new sqlite("../db/$dbuser/strcmp.db");

//check password
if ($password == '') {
	//print success message
	$flag = $sqlite->getflag($dbuser);
	print "Login successful, the flag is <flag>$flag</flag>";
} else {
	print "Login failed. Hint: strcmp behaves, um, interestingly, when given null parameters. <a href='index.html'>Try again</a>";
}
$sqlite->close();
?>
<?php
require_once('class.sqlite.php');
require_once('inc.func.php');
$dbuser = base64_decode($_COOKIE['loggedin']);
$sqlite = new sqlite("db/completion.db");
$challenges = $sqlite->getunsolved($dbuser);
?>
<html>
<head>
	<title>MetaCTF Web | Submit Flag</title>
</head>
<body>
<form action="flagproc.php" method="post">
	<table>
		<tr>
			<td><label for="challenge">Challenge: </label></td>
			<td>
				<select class="select_challenge" name="challenge" id="challenge">
					<? foreach($challenges as $temp) {echo "<option value='$temp'>$temp</option>";} ?>
				</select>
			</td>
		</tr>
		<tr>
			<td><label for="flag">Flag: </label></td>
			<td><input type="text" name="flag" id="flag"></td>
		</tr>
		<tr>
			<td><input type="submit" value="Submit Flag"></td>
		</tr>
	</table>
</form>
</body>
</html>
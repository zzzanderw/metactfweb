<?php
//import files
require_once('class.sqlite.php');
require_once('inc.func.php');

//make login sqlite object
$sqlite = new sqlite("./db/userlogins.db");

//read in POST data
$username = $_POST['username'];
$password = $_POST['password'];

//prep sql query
$sql = "SELECT password FROM users WHERE username = '$username';";

//run sql
$rez = $sqlite->query($sql);

//get array from rez obj
$rezarray = $rez->fetchArray(SQLITE3_NUM);

//compare user pass to db pass
if (strcmp($password, $rezarray[0]) == 0) {
	setcookie("loggedin", base64_encode($username), 0, null, null, false, true);

	//output login success message
	print "Login successful, click <a href='home.html'>here</a> to continue.";
} else {
	print "Login failed, click here <a href='index.php'>here</a> to try again.";
}

//close login sqlite object
$sqlite->close();

?>
<?php
require_once('class.sqlite.php');
require_once('inc.func.php');
$flag = $_POST['flag'];
$challenge = $_POST['challenge'];
$user = base64_decode($_COOKIE['loggedin']);
$completiondb = new sqlite("./db/completion.db");
$challengedb = new sqlite("./db/$user/$challenge.db");
$dbflag = $challengedb->getflag($user);
if ($dbflag == $flag) {
	$completiondb->solvechallenge($user, $challenge);
	echo "You have correctly solved $challenge. Click <a href='home.html'>here</a> to go home.";
} else {
	echo "You have incorrectly solved $challenge. Click <a href='$challenge/'>here</a> to try again.";
}
?>
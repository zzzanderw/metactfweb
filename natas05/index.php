<html>
<head>
	<title>MetaCTF Web | Natas05</title>
</head>
<body>
<?php
if (!array_key_exists('dowewantcookies', $_COOKIE)) {
	setcookie('dowewantcookies', 0);
}
if ($_COOKIE['dowewantcookies'] == 0) {
	echo "Error! We want cookies";
} else if ($_COOKIE['dowewantcookies'] >= 1) {
	require_once('../class.sqlite.php');
	require_once('../inc.func.php');
	$dbuser = base64_decode($_COOKIE['loggedin']);
	$sqlite = new sqlite("../db/$dbuser/natas05.db");
	$flag = $sqlite->getflag($dbuser);
	echo "The flag is <flag>$flag</flag>";
}
?>
<!--The cookie is called dowewantcookies-->
</body>
</html>
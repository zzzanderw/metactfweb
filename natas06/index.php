<html>
<head>
	<title>MetaCTF Web | Natas06</title>
</head>
<body>
	<form action="index.php" method="post">
		<label for="secret">Secret:</label> <input type="text" name="secret" id="secret"><br>
		<input type="submit" value="Submit">
	</form>
	<br>
	<?php
	$secret = 'C8zurutA';
	if (array_key_exists("secret", $_POST)) {
		if ($secret == $_POST['secret']) {
			require_once('../class.sqlite.php');
			require_once('../inc.func.php');
			$dbuser = base64_decode($_COOKIE['loggedin']);
			$sqlite = new sqlite("../db/$dbuser/natas06.db");
			$flag = $sqlite->getflag($dbuser);
			echo "The flag is <flag>$flag</flag>";
		} else {
			echo "Wrong secret";
		}
	}
	?>
<br>
<br>
<br>
<a href="source.txt">View source</a>
</body>
</html>
<?php
require_once('../class.sqlite.php');
require_once('../inc.func.php');

//get dbname, username and password from post request
$username = $_POST['username'];
$password = $_POST['password'];
$dbuser = base64_decode($_COOKIE['loggedin']);

//check to make sure they aren't trying to run DROP or SELECT
$drop = stristr($username, 'DROP');
$select = stristr($username, 'SELECT');
if ($drop !== FALSE || $select !== FALSE) {
	exit("You need to use an INSERT INTO statement.<br><a href='index.html'>Try again.</a>");
}

//define sqlite object
$sqlite = new sqlite("../db/$dbuser/cred_inject.db");

//prepare query SQL
$sql = "SELECT password FROM users WHERE username ='" . $username . "';";

//run sql query against db
$sqlite->exec($sql);
$rez = $sqlite->query($sql);

//pull out data from result obj
$data = null;
while($row = $rez->fetchArray(SQLITE3_NUM)) {
	foreach ($row as $temp) {
		$data[] = $temp;
	}
}

//make sure neither strcmp param is null
if ($password != null) {
	//check password
	foreach($data as $temp2) {
		if (strcmp($password, $temp2) == 0) {
			//print success message
			$flag = $sqlite->getflag($dbuser);
			print "Login successful, the flag is <flag>$flag</flag>";
			break;
		} else {
			print "Login failed, click <a href='index.html'>here</a> to try again";
		}
	}
} else {
	print "You need to enter a password! <br><a href='index.html'>Try again.</a>";
}
$sqlite->close();
?>
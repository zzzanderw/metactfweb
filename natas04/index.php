<html>
<head>
	<title>MetaCTF Web | Natas04</title>
</head>
<body>
<?php
$headers = apache_request_headers();
if ($headers['Referer'] == "http://www.ilovewebsecurity.com" || $headers['Referer'] == "http://www.ilovewebsecurity.com/") {
	require_once('../class.sqlite.php');
	require_once('../inc.func.php');
	$dbuser = base64_decode($_COOKIE['loggedin']);
	$sqlite = new sqlite("../db/$dbuser/natas04.db");
	$flag = $sqlite->getflag($dbuser);
	echo "The flag is <flag>$flag></flag>";
} else {
	echo "Error! You must be coming from http://www.ilovewebsecurity.com";
}
?>
</body>
</html>
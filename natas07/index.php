<html>
<head>
	<title>MetaCTF Web | Natas07</title>
</head>
<body>
<a href="index.php?file=home">Home</a>  <a href="index.php?file=about">About</a>
<br>
<?php
if (array_key_exists('file', $_GET)) {
	$file = $_GET['file'];
	if ($file == '/etc/flags/natas07') {
		$user = base64_decode($_COOKIE['loggedin']);
		require_once('../class.sqlite.php');
		require_once('../inc.func.php');
		$sqlite = new sqlite("../db/$user/natas07.db");
		$flag = $sqlite->getflag($user);
		echo "The flag is <flag>$flag</flag>";
	} elseif ($file == 'home') {
		echo 'You are at the home page';
	} elseif ($file == 'about') {
		echo 'You are at the about page';
	} else {
		echo "You have tried to go to an illegal page.</br>Attempted page accessed: $file";
	}
}
?>
<!--The flag is located at /etc/flags/natas07-->
</body>
</html>
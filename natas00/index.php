<?php
require_once('../class.sqlite.php');
require_once('../inc.func.php');
$dbuser = base64_decode($_COOKIE['loggedin']);
$sqlite = new sqlite("../db/$dbuser/natas00.db");
$flag = $sqlite->getflag($dbuser);
?>
<html>
<head>
	<title>MetaCTF Web | Natas00</title>
</head>
<body>
The flag is on this page.
<!--The flag is <flag><? echo $flag; ?></flag>-->
<!--NOTE: The <flag> and the </flag> are not a part of the flag. The flag is the 16 alphanumerics between the <flag> and the </flag> (this is true for all of the challenges)-->
</body>
</html>
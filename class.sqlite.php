<?php
Class sqlite extends SQLite3 {
	function __construct($dbfilepath) {
		$this->open($dbfilepath);
	}
	function flag_table_gen() {
		$sql = "CREATE TABLE IF NOT EXISTS flags (username TEXT, flag TEXT);";
		$this->exec($sql);
	}
	function addflag($user, $flag) {
		$sql = "INSERT INTO flags VALUES ('$user', '$flag')";
		$this->exec($sql);
	}
	function getflag($user) {
		$sql = "SELECT flag FROM flags WHERE username = '$user';";
		$rez = $this->query($sql);
		$temp = $rez->fetchArray(SQLITE3_NUM);
		return $temp[0];
	}
	function getsolved($user) {
		$sql = "SELECT challenge FROM completed WHERE status = 1 AND username = '$user';";
		$rez = $this->query($sql);
		$ret = null;
		while($row = $rez->fetchArray(SQLITE3_NUM)) {
			foreach ($row as $temp) {
				$ret[] = $temp;
			}
		}
		return $ret;
	}
	function getunsolved($user) {
		$sql = "SELECT challenge FROM completed WHERE status = 0 AND username = '$user';";
		$rez = $this->query($sql);
		$ret = null;
		while($row = $rez->fetchArray(SQLITE3_NUM)) {
			foreach ($row as $temp) {
				$ret[] = $temp;
			}
		}
		return $ret;
	}
	function addchallenge($user, $challenge) {
		$sql = "INSERT INTO completed VALUES ('$user', '$challenge', 0)";
		$this->exec($sql);
	}
	function solvechallenge($user, $challenge) {
		$sql = "UPDATE completed SET status = 1 WHERE username = '$user' AND challenge = '$challenge';";
		$this->exec($sql);
	}
}
?>
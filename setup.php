<?php
//import files
require_once('class.sqlite.php');
require_once('inc.func.php');

//make an array with all usernames from file
$userarray = file('./users', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
echo "read in users file\n";

//make an array with all challenges from file
$challengearray = file('./challenges', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
echo "read in challenge file\n";

//setup counter
$numusers = count($userarray);
$c = 0;

//make challenges.html
$challengefile = fopen('challenges.html', 'w');

//prep data
$data1 = "<html>\n<head>\n<title>MetaCTF Web | Challenges Listing</title>\n</head>\n<body>";
$data2 = null;
foreach ($challengearray as $challenge) {
	$data2 .= "<a href='$challenge/'>$challenge</a><br>\n";
}
$data3 = "</body></html>";

//write file
fwrite($challengefile, $data1);
fwrite($challengefile, $data2);
fwrite($challengefile, $data3);

//close file
fclose($challengefile);

echo "made challenges.html\n";

//make db/
mkdir("db/");

//change perms on db/
shell_exec("chgrp -R www-data db/");
echo "created db folder w/ correct perms\n";

//define database
$sqlite = new sqlite("./db/completion.db");
echo "generated completion database\n";

//prep sql
$sql = "CREATE TABLE IF NOT EXISTS completed (username TEXT, challenge TEXT, status INTEGER)";

//run sql
$sqlite->exec($sql);

//close sqlite obj
$sqlite->close();

foreach ($userarray as $user) {
	//add a line break
	echo "\n";

	//increment counter
	$c++;

	//make directories for each user
	mkdir("./db/$user", 0755);

	//give status update
	echo "made directory db/$user\n";

	//make challenge dbs
	foreach ($challengearray as $chname) {
		echo "\n";

		//make a sqlite databse for each user
		$sqlite = new sqlite("./db/$user/$chname.db");

		//give status update
		echo "made challenge database at db/$user/$chname.db\n";

		//make user table
		$sqlite->flag_table_gen();

		//generate password
		$flag = password_gen(16, $user, $chname);

		//return password
		echo "Flag for user $user on challenge $chname is $flag\n";

		//insert username/password into database
		$sqlite->addflag($user, $flag);

		//give status update
		echo "Added flag to challenge database\n";

		if ($chname == 'cred_inject') {
			$sql = 'CREATE TABLE users (username TEXT, password TEXT);';
			$sqlite->exec($sql);
		}

		//close sqlite db
		$sqlite->close();

		//open completion database
		$sqlite = new sqlite("./db/completion.db");

		//added user uncompleted challenge to db
		$sqlite->addchallenge($user, $chname);

		//give status update
		echo "Added challenge $chname for user $user to completion db\n";
	}

	//fix permissions of db
	shell_exec("chmod -R 775 ./db/$user/");
	shell_exec("chgrp -R www-data ./db/$user/");

	//give status update
	echo "fixed perms for $user\n";

	//give status update
	echo "done $c of $numusers\n";
}

//make sqlite obj
$sqlite = new sqlite("./db/userlogins.db");
echo "created system login database\n";

//prep sql
$sql = "CREATE TABLE IF NOT EXISTS users (username TEXT, password TEXT)";

//run the code to make the table
$sqlite->exec($sql);

//setup counter
$c = 0;
$numusers = count($userarray);

foreach ($userarray as $user) {
	//increment counter
	$c++;

	//generate password
	$password = password_gen(8, $user, "login");
	echo "system login for $user is $password\n";

	//write sql query
	$sql = "INSERT INTO users VALUES ('$user', '$password')";

	//execute sql
	$sqlite->exec($sql);
}

//fix permissions of db
shell_exec("chmod -R 775 ./db/*");
shell_exec("chgrp -R www-data ./db/*");
echo "corrected permissions for all databases in db/\n";

//close sqlite object
$sqlite->close();

?>
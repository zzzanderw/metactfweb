<?php
if (array_key_exists('loggedin', $_COOKIE)) {
	header('Location: http://totoro.cs.pdx.edu/home.html');
}
?>
<html>
<head>
	<title>Login to MetaCTF Web</title>
</head>
<body>
<h3>
	Welcome to MetaCTF Web: A scaffolded metamorphic CTF series, with a focus on web security.<br>
	The objective is to find the 'flag' for each level, and then submit the flags for credit<br>
</h3>
<form action="loginproc.php" method="post" id="0">
	<table>
		<tr>
			<td><label form="0" for="username">Username: </label></td>
			<td><input type="text" name="username" id="username"></td>
		</tr>
		<tr>
			<td><label form="0" for="password">Password: </label></td>
			<td><input type="password" name="password" id="password"></td>
		</tr>
		<tr>
			<td><input type="submit" value="Login"></td>
		</tr>
	</table>
</form>
<br>
<img src="totoro.jpg">
</body>
</html>
# README #

###MetaCTF Web###
MetaCTF Web is a scaffolded, jeopardy-style Capture-the-flag set of challenges that are designed for classroom usage. Most of the challenges are based on the [Natas CTF](http://overthewire.org/wargames/natas/) from [OverTheWire](http://overthewire.org/), and some are developed at PSU.

###Instructors###
Instructors who wish to use MetaCTF Web in a class should consult the INSTALL file, located at the root level. This file will detail how to setup MetaCTF Web for your class in a painless, easy way.

###Developers###
Developers who wish to contribute to MetaCTF Web should consult the DEVELOP file, located at the root level. This file will detail the underlying functions of the system and explain how to get setup to work on the project.


###Questions###
If you have any questions, feel free to email me: zander (at) zanderwork (dot) com